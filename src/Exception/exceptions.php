<?php

/**
 * This file is part of the YetORM library.
 *
 * Copyright (c) 2014 Jaroslav Hranička
 * Copyright (c) 2013, 2014 Petr Kessler (http://kesspess.1991.cz)
 *
 * @license  MIT
 * @link     https://bitbucket.org/hranicka/yetorm
 * @link     https://github.com/uestla/YetORM
 */

namespace YetORM\Exception;

class InvalidArgumentException extends \InvalidArgumentException
{

}

class InvalidStateException extends \RuntimeException
{

}

class MemberAccessException extends \LogicException
{

}

class NotSupportedException extends \LogicException
{

}

class RowNotSetException extends InvalidStateException
{

}

class AnnotationException extends InvalidArgumentException
{

}
