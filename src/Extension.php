<?php

namespace YetORM;

use Nette;
use Nette\DI\CompilerExtension;

class Extension extends CompilerExtension
{

	/** @var array */
	private $defaults = [
		'storage' => NULL,
	];

	public function afterCompile(Nette\PhpGenerator\ClassType $class)
	{
		$initialize = $class->methods['initialize'];
		$config = $this->getConfig($this->defaults);

		if ($config['storage']) {
			$initialize->addBody('\YetORM\Reflection\EntityReflection::setCacheStorage($this->getService(?));', [
				$config['storage'],
			]);
		}
	}

}
