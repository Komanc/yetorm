<?php

/**
 * This file is part of the YetORM library.
 *
 * Copyright (c) 2014 Jaroslav Hranička
 * Copyright (c) 2013, 2014 Petr Kessler (http://kesspess.1991.cz)
 *
 * @license  MIT
 * @link     https://bitbucket.org/hranicka/yetorm
 * @link     https://github.com/uestla/YetORM
 */

namespace YetORM;

use Nette;
use Nette\Database\Table\ActiveRow as NActiveRow;
use Nette\Database\Table\GroupedSelection as NGroupedSelection;

class Row
{

	/** @var NActiveRow|null */
	private $row;

	/** @var array */
	private $values = [];

	/** @var array */
	private $modified = [];

	/** @param NActiveRow|array|null $row */
	public function __construct($row = NULL)
	{
		if (is_array($row)) {
			$this->modified = $row;
		} else {
			$this->row = $row;
		}
	}

	public function __clone()
	{
		if ($this->row) {
			$this->row->toArray(); // access all columns
			$this->row = clone $this->row;
		}
	}

	public function copy()
	{
		$clone = clone $this;

		// Copy values
		$native = $clone->getNative();
		$values = ($native) ? $native->toArray() : [];

		// Remove ActiveRow instance
		$clone->removeNative();

		// Fill values again
		foreach ($values as $key => $value) {
			$clone->__set($key, $value);
		}

		return $clone;
	}

	public function toArray()
	{
		$values = $this->modified + $this->values;
		if ($this->row) {
			$values += $this->row->toArray();
		}

		foreach ($values as & $value) {
			// run toArray also for loaded relations
			if ($value instanceof NActiveRow) {
				$value = $value->toArray();
			}

			// fixes serialize issue (invalid references?)
			if ($value instanceof \DateTime) {
				$value = clone $value;
			}
		}

		return $values;
	}

	/** @return bool */
	public function hasNative()
	{
		return $this->row !== NULL;
	}

	/** @return bool */
	public function isPersisted()
	{
		return $this->hasNative() && !count($this->modified);
	}

	/** @return NActiveRow|NULL */
	public function getNative()
	{
		return $this->row;
	}

	/**
	 * @param NActiveRow $row
	 * @return Row
	 */
	public function setNative(NActiveRow $row)
	{
		$this->reload($row);
		return $this;
	}

	/**
	 * @return $this
	 */
	public function removeNative()
	{
		$this->row = NULL;
		return $this;
	}

	/**
	 * @param string $key
	 * @param string $throughColumn
	 * @return NActiveRow|NULL
	 */
	public function ref($key, $throughColumn = NULL)
	{
		$this->checkPersistence();
		return $this->row->ref($key, $throughColumn);
	}

	/**
	 * @param string $key
	 * @param string $throughColumn
	 * @return NGroupedSelection
	 */
	public function related($key, $throughColumn = NULL)
	{
		$this->checkPersistence();
		return $this->row->related($key, $throughColumn);
	}

	/** @return array */
	public function getModified()
	{
		return $this->modified;
	}

	/** @return bool */
	public function update()
	{
		$this->checkPersistence();

		$status = TRUE;
		if (!$this->isPersisted()) {
			$status = $this->row->update($this->modified);
			$this->reload($this->row);
		}

		return $status;
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function & __get($name)
	{
		if (array_key_exists($name, $this->modified)) {
			return $this->modified[$name];
		}

		if (array_key_exists($name, $this->values)) {
			return $this->values[$name];
		}

		if ($this->row === NULL) {
			return $this->row;
		}

		$value = $this->values[$name] = $this->row->$name;
		return $value;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->modified[$name] = $value;
	}

	public function get($name)
	{
		return $this->__get($name);
	}

	public function set($name, $value)
	{
		$this->__set($name, $value);
		return $this;
	}

	public function has($name)
	{
		return $this->__isset($name);
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset($this->modified[$name])
		|| isset($this->values[$name])
		|| isset($this->row->$name);
	}

	/**
	 * @throws Exception\InvalidStateException
	 */
	private function checkPersistence()
	{
		if ($this->row === NULL) {
			throw new RowNotSetException('Row not set yet.');
		}
	}

	/**
	 * @param NActiveRow $row
	 * @return void
	 */
	private function reload(NActiveRow $row)
	{
		$this->row = $row;
		$this->modified = $this->values = [];
	}

}

class RowNotSetException extends \YetORM\Exception\RowNotSetException
{

}
