<?php

/**
 * This file is part of the YetORM library.
 *
 * Copyright (c) 2014 Jaroslav Hranička
 * Copyright (c) 2013, 2014 Petr Kessler (http://kesspess.1991.cz)
 *
 * @license  MIT
 * @link     https://bitbucket.org/hranicka/yetorm
 * @link     https://github.com/uestla/YetORM
 */

namespace YetORM;

use Nette\Database\Table\ActiveRow as NActiveRow;
use Nette\Reflection as NReflection;
use Nette\Utils\Strings;
use YetORM\Exception\InvalidStateException;
use YetORM\Reflection;

/**
 * @property-read int|null $id
 *
 * @method int|null getId()
 */
abstract class Entity implements \IteratorAggregate, \ArrayAccess, \Serializable
{

	/** @var callable[] */
	public $onPersist = [];

	/** @var callable[] */
	public $onDelete = [];

	/** @var Row */
	protected $row;

	/** @var array */
	private $values = [];

	/** @var EntitySleeper */
	private $sleeper;

	/**
	 * @return Reflection\EntityType
	 */
	public static function getEntityType()
	{
		return Reflection\EntityReflection::getReflection(self::getClassName());
	}

	/**
	 * @return NReflection\ClassType
	 */
	public static function getReflection()
	{
		return self::getEntityType()->getReflection();
	}

	/**
	 * It's useful for ::getOne, ::getMany, etc.
	 * @return string
	 */
	public static function getClassName()
	{
		return get_called_class();
	}

	/**
	 * @param NActiveRow|array|null $row
	 */
	public function __construct($row = NULL)
	{
		$this->createRow($row);
		if (!$this->row->hasNative()) {
			$this->init();
		}
	}

	public function __clone()
	{
		if ($this->row) {
			$this->row = clone $this->row;
		}
	}

	/**
	 * Resets specified column, by default ID.
	 * It's useful when saving cloned Entity with unique key.
	 * @param string $column
	 * @param int $depth
	 * @return $this
	 */
	public function reset($column = 'id', $depth = 99)
	{
		if ($this->row) {
			$this->row->$column = NULL;
		}

		// Back compatibility fix
		if (is_bool($depth)) {
			trigger_error('::reset $depth must be integer, boolean given.', E_USER_DEPRECATED);
			$depth = ($depth) ? 99 : 0;
		}

		if ($depth--) {
			foreach ($this->toArray() as $key => $value) {
				if ($value instanceof Entity) {
					$value->reset($column, $depth);
					$this->$key = $value;
				}
			}
		}

		return $this;
	}

	/**
	 * Returns Entity clone without ActiveRow reference inside.
	 * @return Entity
	 */
	public function copy()
	{
		$clone = clone $this;
		$clone->loadReferences();
		$clone->fromRow($clone->toRow()->copy());
		return $clone;
	}

	/**
	 * @param string $name
	 * @param array $args
	 * @return mixed
	 * @throws Exception\MemberAccessException
	 */
	public function __call($name, $args)
	{
		$getter = $this->nameOfGetter($name);
		if (static::getEntityType()->getEntityProperty($getter)) {
			$value = $this->__get($getter);
			return $value;
		}

		$setter = $this->nameOfSetter($name);
		if (static::getEntityType()->getEntityProperty($setter)) {
			$value = $this->__set($setter, array_shift($args));
			return $value;
		}

		$class = get_class($this);
		throw new Exception\MemberAccessException("Call to undefined method $class::$name().");
	}

	/**
	 * @param string $name
	 * @return mixed
	 * @throws Exception\MemberAccessException
	 */
	public function & __get($name)
	{
		$prop = static::getEntityType()->getEntityProperty($name);
		$methodExists = method_exists($this, $this->formatGetter($name));

		if (($prop && $methodExists) || ($methodExists)) {
			$value = call_user_func([$this, $this->formatGetter($name)]);
			return $value;

		} elseif ($prop) {
			$value = $prop->setType($this->row->{$prop->getColumn()});
			return $value;
		}

		$class = get_class($this);
		throw new Exception\MemberAccessException("Cannot read an undeclared property $class::\$$name.");
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return $this
	 * @throws Exception\MemberAccessException
	 */
	public function __set($name, $value)
	{
		$prop = static::getEntityType()->getEntityProperty($name);
		if ($prop && method_exists($this, $this->formatSetter($name))) {
			call_user_func([$this, $this->formatSetter($name)], $value);
			return $this;

		} elseif ($prop && !$prop->isReadOnly()) {
			if ($prop instanceof Reflection\MethodProperty) {
				$this->row->{$prop->getName()} = $value;
			} elseif ($prop instanceof Reflection\AnnotationProperty) {
				$prop->checkType($value);
				$this->row->{$prop->getColumn()} = $value;
			}

			return $this;
		}

		$class = get_class($this);
		throw new Exception\MemberAccessException("Cannot write to an undeclared property $class::\$$name.");
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name)
	{
		$prop = static::getEntityType()->getEntityProperty($name);
		if ($prop && method_exists($this, $this->formatGetter($name))) {
			$value = call_user_func([$this, $this->formatGetter($name)]);
			return $value !== NULL;

		} elseif ($prop) {
			return $this->__get($name) !== NULL;
		}

		return FALSE;
	}

	/**
	 * @param string $name
	 * @return void
	 * @throws Exception\NotSupportedException
	 */
	public function __unset($name)
	{
		throw new Exception\NotSupportedException;
	}

	final public function serialize()
	{
		$this->sleeper = new EntitySleeper($this);
		return serialize($this->sleeper);
	}

	final public function unserialize($serialized)
	{
		$sleeper = unserialize($serialized);
		$this->wakeUp($sleeper);
	}

	/**
	 * @param EntitySleeper $sleeper
	 * @param EntityReference|null $reference
	 * @internal
	 */
	final public function wakeUp(EntitySleeper $sleeper, EntityReference $reference = NULL)
	{
		$this->sleeper = $sleeper;
		$values = $sleeper->getValues($reference);

		$this->values = $values['values'];
		$this->createRow($values['row']);
	}

	/**
	 * @return Row
	 */
	final public function toRow()
	{
		return $this->row;
	}

	/**
	 * @param Row $row
	 * @return $this
	 */
	final public function fromRow(Row $row)
	{
		$this->row = $row;
		return $this;
	}

	/**
	 * Looks for all public get* methods and @property[-read] annotations
	 * and returns associative array with corresponding values
	 *
	 * @return array
	 */
	final public function toArray()
	{
		return $this->loadReferences(TRUE);
	}

	/**
	 * @param bool $exceptions
	 * @return array
	 * @throws \Exception
	 */
	final public function loadReferences($exceptions = FALSE)
	{
		$data = [];

		$ref = static::getEntityType();
		foreach ($ref->getEntityProperties() as $name => $property) {
			try {
				if ($property instanceof Reflection\MethodProperty) {
					$value = $this->{'get' . $name}();
				} else {
					$value = $this->$name;
				}
			} catch (\Exception $e) {
				if ($exceptions) {
					throw $e;
				} else {
					continue;
				}
			}

			// expand and check collection
			if ($value instanceof Collection) {
				/** @var Collection|Entity[] $collection */
				$collection = $value;
				$value = [];

				foreach ($collection as $entity) {
					$value[] = $entity;
				}
			}

			$data[$name] = $value;
		}

		return $data;
	}

	/**
	 * @return $this
	 */
	public function refresh()
	{
		$this->values = [];
		return $this;
	}

	public function getIterator()
	{
		return new \ArrayIterator($this->toArray());
	}

	public function offsetExists($offset)
	{
		try {
			$this->__get($offset);
			return TRUE;
		} catch (Exception\MemberAccessException $e) {
			return FALSE;
		}
	}

	public function offsetGet($offset)
	{
		return $this->__get($offset);
	}

	public function offsetSet($offset, $value)
	{
		$this->__set($offset, $value);
	}

	public function offsetUnset($offset)
	{
		$this->__unset($offset);
	}

	/**
	 * @param int|null $maxDepth Max. depth to which Entities are expanded.
	 * @return array
	 */
	final public function toArrayRecursive($maxDepth = NULL)
	{
		return $this->expandEntity($this->toArray(), [$this], $maxDepth);
	}

	/**
	 * @param array $data
	 * @param bool $exceptions
	 * @return $this
	 * @throws Exception\InvalidArgumentException
	 */
	final public function fromArray($data, $exceptions = FALSE)
	{
		foreach ($data as $setter => $value) {
			try {
				$this->__set($setter, $value);
			} catch (Exception\MemberAccessException $e) {
				if ($exceptions) {
					throw new Exception\InvalidArgumentException("Attribute '$setter' is not writable.");
				}
			}
		}

		return $this;
	}

	/**
	 * Method is called only when Row is empty
	 * (new empty entity was created)
	 */
	protected function init()
	{

	}

	/**
	 * @param string $column
	 * @param Entity|null $entity
	 * @return $this
	 */
	protected function setOne($column, Entity $entity = NULL)
	{
		$key = $this->nameOfSetter($this->getCallerMethod());
		$this->row->$column = ($entity) ? $entity->getId() : NULL;
		$this->setEntityValue($entity, $key);

		return $this;
	}

	/**
	 * @param mixed $value
	 * @param string|null $key
	 * @return $this
	 */
	protected function setEntityValue($value, $key = NULL)
	{
		if (!$key) {
			$key = $this->nameOfSetter($this->getCallerMethod());
		}

		$this->values[$key] = $value;
		return $this;
	}

	/**
	 * @param string|callable $entity
	 * @param string $relTable
	 * @param string $entityTable
	 * @param string $throughColumn
	 * @param array $criteria
	 * @return Collection
	 */
	protected function getMany($entity, $relTable, $entityTable = NULL, $throughColumn = NULL, $criteria = [])
	{
		$key = $this->nameOfGetter($this->getCallerMethod());
		return $this->getEntityValue($key, function () use ($relTable, $entity, $entityTable, $throughColumn, $criteria) {
			// Don't fall eg. for serialized entity which has been extended with new method.
			if (!$this->row->hasNative()) {
				return new EntityCollection();
			}

			$selection = $this->row->related($relTable);
			if ($criteria) {
				$selection->where($criteria);
			}

			return new EntityCollection($selection, $entity, $entityTable, $throughColumn);
		});
	}

	/**
	 * @param string $entity
	 * @param string $relTable
	 * @param string|null $throughColumn
	 * @param bool $required
	 * @return Entity|null
	 * @throws InvalidStateException
	 */
	protected function getOne($entity, $relTable, $throughColumn = NULL, $required = FALSE)
	{
		$fallback = function () use ($entity, $relTable, $throughColumn, $required) {
			if ($required || $this->row->hasNative()) {
				$row = $this->row->ref($relTable, $throughColumn);
			} else {
				$row = NULL;
			}

			if ($row) {
				return new $entity($row);
			} elseif ($required) {
				throw new InvalidStateException("Reference to '$relTable' was not found.");
			}

			return NULL;
		};

		$key = $this->nameOfGetter($this->getCallerMethod());
		return $this->getEntityValue($key, $fallback);
	}

	/**
	 * @param string|null $key
	 * @param callable|null $fallback
	 * @return mixed
	 */
	protected function getEntityValue($key = NULL, callable $fallback = NULL)
	{
		if (!$key) {
			$key = $this->nameOfGetter($this->getCallerMethod());
		}

		if (array_key_exists($key, $this->values)) {
			$value = $this->values[$key];
			return $this->wakeUpValue($value);
		}

		$value = ($fallback) ? call_user_func($fallback) : NULL;
		$this->setEntityValue($value, $key);
		return $value;
	}

	/**
	 * @param array $values
	 * @param Entity[] $ignored
	 * @param int|null $maxDepth
	 * @param int $depth
	 * @return array
	 */
	private function expandEntity(array $values, array $ignored = [], $maxDepth = NULL, $depth = 0)
	{
		foreach ($values as $key => & $value) {
			// check ignored objects (infinite recursion loop workaround)
			foreach ($ignored as $ignore) {
				if ($value instanceof $ignore && $value->getId() === $ignore->getId()) {
					unset($values[$key]);
					continue 2;
				}
			}

			// expand Entity if $maxDepth has not been reached yet
			if ($value instanceof Entity) {
				if ($maxDepth !== NULL && $depth > $maxDepth) {
					unset($values[$key]);
					continue;
				}

				$value = $this->expandEntity($value->toArray(), array_merge($ignored, [$value]), $maxDepth, $depth + 1);
			}

			// check for collections and expand them if $maxDepth has not been reached yet
			if (is_array($value) && $value) {
				$value = $this->expandEntity($value, $ignored, $maxDepth, $depth + 1);
			}
		}

		return $values;
	}

	/**
	 * @param mixed $value
	 * @return Entity
	 */
	private function wakeUpValue($value)
	{
		if ($this->sleeper) {
			if ($value instanceof EntityReference) {
				$value = $this->sleeper->wakeUp($value);
			}

			if (is_array($value)) {
				foreach ($value as & $subValue) {
					if ($subValue instanceof EntityReference) {
						$subValue = $this->sleeper->wakeUp($subValue);
					}
				}
			}
		}

		return $value;
	}

	/**
	 * IMPORTANT! Call only directly due to debug_backtrace implementation!
	 * @return string
	 */
	private function getCallerMethod()
	{
		return debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3)[2]['function'];
	}

	/**
	 * @param NActiveRow|array|null $row
	 * @return Row
	 */
	private function createRow($row = NULL)
	{
		$this->row = new Row($row);
		return $this->row;
	}

	private function formatGetter($name)
	{
		return 'get' . ucfirst($name);
	}

	private function formatSetter($name)
	{
		return 'set' . ucfirst($name);
	}

	private function nameOfGetter($name)
	{
		if (Strings::startsWith($name, 'get')) {
			return lcfirst(Strings::substring($name, 3));
		}

		return NULL;
	}

	private function nameOfSetter($name)
	{
		if (Strings::startsWith($name, 'set')) {
			return lcfirst(Strings::substring($name, 3));
		}

		return NULL;
	}

}
