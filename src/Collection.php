<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 *
 * @license MIT
 * @link https://bitbucket.org/hranicka/yetorm
 */

namespace YetORM;

interface Collection extends \Iterator, \Countable
{

	const ASC = FALSE;
	const DESC = TRUE;

	/**
	 * @return array
	 */
	public function toArray();

	/**
	 * @return $this
	 */
	public function resetOrder();

	/**
	 * @param string|array $column
	 * @param bool $direction
	 * @return $this
	 */
	public function orderBy($column, $direction = NULL);

	/**
	 * @param int $limit
	 * @param int $offset
	 * @return $this
	 */
	public function limit($limit, $offset = NULL);

	/**
	 * @param $column
	 * @return int
	 */
	public function countDistinct($column);

}
