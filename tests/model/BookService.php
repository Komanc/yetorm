<?php

namespace Model\Services;

use Model;
use YetORM\Collection;

class BookService
{

	/** @var Model\Repositories\BookRepository */
	protected $repository;

	public function __construct(Model\Repositories\BookRepository $repository)
	{
		$this->repository = $repository;
	}

	/** @return Collection */
	public function getLatest()
	{
		return $this->repository->getAll()
			->orderBy('written', Collection::DESC)
			->limit(3);
	}

}
