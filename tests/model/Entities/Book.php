<?php

namespace Model\Entities;

use Nette\Utils\DateTime;
use YetORM;

/**
 * @property-read int $id
 * @property string $bookTitle -> book_title
 * @property Author $author
 * @property DateTime|null $written
 * @property bool $available
 *
 * @method string getBookTitle()
 * @method Book setBookTitle(string $title)
 *
 * @method DateTime|null getWritten()
 * @method Book setWritten(DateTime $written = NULL)
 */
class Book extends YetORM\Entity
{

	/** @var Tag[] */
	protected $addedTags = [];

	/** @var Tag[] */
	protected $removedTags = [];

	/**
	 * @param string $name
	 * @return $this
	 */
	public function addTag($name)
	{
		$tag = new Tag;
		$tag->name = $name;
		$this->addedTags[] = $tag;
		return $this;
	}

	/**
	 * @return Tag[]
	 * @internal
	 */
	public function getAddedTags()
	{
		$tmp = $this->addedTags;
		return $tmp;
	}

	/**
	 * @param string $name
	 * @return $this
	 */
	public function removeTag($name)
	{
		$tag = new Tag;
		$tag->name = $name;
		$this->removedTags[] = $tag;
		return $this;
	}

	/**
	 * @return Tag[]
	 * @internal
	 */
	public function getRemovedTags()
	{
		$tmp = $this->removedTags;
		return $tmp;
	}

	/**
	 * @return Author
	 */
	public function getAuthor()
	{
		return $this->getOne('\Model\Entities\Author', 'author', 'author_id');
	}

	/**
	 * @param Author
	 * @return Book
	 */
	public function setAuthor(Author $author)
	{
		return $this->setOne('author_id', $author);
	}

	/**
	 * @return YetORM\Collection
	 */
	public function getTags()
	{
		return $this->getMany('Model\Entities\Tag', 'book_tag', 'tag');
	}

	/**
	 * @return string
	 * @internal
	 */
	public function getImagePath()
	{
		return '/' . $this->id . '.jpg';
	}

	/**
	 * @return string
	 * @no-serialize
	 */
	public function getNoSerialize()
	{
		return 'bump';
	}

}
