<?php

namespace Model\Entities;

use YetORM;

/**
 * @property string $web
 * @property mixed|null $customData
 *
 * @method string getWeb()
 */
class Author extends Person
{

	/** @return YetORM\Collection */
	public function getBooks()
	{
		return $this->getMany('Model\Entities\Book', 'book');
	}

	/**
	 * @param mixed $data
	 * @return $this
	 */
	public function setCustomData($data)
	{
		return $this->setEntityValue($data);
	}

	/**
	 * @return mixed
	 */
	public function getCustomData()
	{
		return $this->getEntityValue();
	}

}
