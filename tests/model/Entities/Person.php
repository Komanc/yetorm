<?php

namespace Model\Entities;

use Nette;
use YetORM;

/**
 * @property-read int $id
 * @property string $name
 * @property-read Nette\Utils\DateTime|null $born
 */
abstract class Person extends YetORM\Entity
{

	/**
	 * @return string
	 */
	public function getName()
	{
		return Nette\Utils\Strings::capitalize($this->row->name);
	}

}
