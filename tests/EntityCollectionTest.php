<?php

/**
 * Copyright © 2015 Jaroslav Hranička <hranicka@outlook.com>
 *
 * @license MIT
 * @link https://bitbucket.org/hranicka/yetorm
 */

namespace Tests;

use YetORM\EntityCollection;

class EntityCollectionTest extends \PHPUnit_Framework_TestCase
{

	public function testEmptyBasic()
	{
		$collection = new EntityCollection();

		$this->assertSame(0, $collection->count());
		$this->assertSame(0, $collection->countDistinct('id'));
		$this->assertSame([], iterator_to_array($collection));
		$this->assertSame([], $collection->toArray());
	}

	public function testEmptyQuery()
	{
		$collection = new EntityCollection();
		$collection->orderBy('boo')->limit(200, 10);

		$this->assertSame(0, $collection->count());
		$this->assertSame(0, $collection->countDistinct('id'));
		$this->assertSame([], iterator_to_array($collection));
		$this->assertSame([], $collection->toArray());
	}

}
